# COMBO for Ancient Greek

This folder contains the code necessary to automatically postag, lemmatize, and
parse Ancient Greek using COMBO. More precisely, here you find:

1. COMBO parser (this has been downloaded from https://github.com/360er0/COMBO)
2. A model trained on the Ancient Greek Dependency Treebank (LDT) 2.1 (i.e., `AncientGreekModel100.pkl` in `COMBO-agdt`) + the code/files necessary
   to make the training reproducible (see the file `command.txt` in `COMBO-agdt`). The corpus division into train/dev/test data has been done via `01_to-conllu.xq` in `COMBO-agdt`.

# Known issues

Annotations within the Ancient Greek Treebank should be made more consistent, including tokenization (especially of conjunctions, such as, for example, οὔτε).

# Results

During the training phase, the COMBO parser has been set to perform 100 epochs.

Validation set accuracies for the last epoch:
UAS: 0.7275181723779854 <br/>
LAS: 0.652128764278297 <br/>
LEMMA: 0.8926791277258567 <br/>
POS: 0.9394600207684319 <br/>
XPOS: 0.8573727933541018 <br/>
FEAT: 0.8952751817237798 <br/>

Test set accuracies: <br/>
UAS: 0.714722654224987 <br/>
LAS: 0.6355106272680145 <br/>
LEMMA: 0.889476412649041 <br/>
POS: 0.9403836184551582 <br/>
XPOS: 0.8540694660445827 <br/>
FEAT: 0.8947641264904095 <br/>

# License

The license for COMBO parser is CC BY-NC-SA 4.0 (see https://github.com/360er0/COMBO).
The AGDT model/code at point 2 + all the code is also released underCC BY-NC 4.0.
