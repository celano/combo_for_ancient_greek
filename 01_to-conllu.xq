xquery version "3.1" encoding "utf-8"; (: BaseX 9.2.1 :)

declare function local:conllu($a) 
{
  for $u in $a//sentence
  
  return
     "# " || $u/@document_id || " " || $u/@subdoc || "&#xA;" ||
     "# " || $u/@id || "&#xA;" || 
     string-join(
     for $t2 in $u/word
     let $lo := fn:string-join(
                       for $u at $c in 
               string-to-codepoints(substring(replace($t2/@postag, "_", "-"),2))
                       where codepoints-to-string($u) != "-"
                       return
                       "key" || $c || "=" || codepoints-to-string($u),
                        "|") 
     return
     if ($t2/@id = $u/word[last()]/@id) 
     then 
       fn:string-join(($t2/@id, $t2/@form, replace($t2/@lemma, "1|2", ""), 
                       substring(replace($t2/@postag, "_", "-"),1,1 ),
                       replace($t2/@postag, "_", "-"), 
                       if ($lo) then $lo else "_",
                       $t2/@head,
                       $t2/@relation,
                       "_",
                       "_" 
                      ), "&#x9;") || "&#xA;"
     else
       fn:string-join(($t2/@id, $t2/@form, replace($t2/@lemma, "1|2", ""), 
                       substring(replace($t2/@postag, "_", "-"),1,1 ),
                       replace($t2/@postag, "_", "-"), 
                       if ($lo) then $lo else "_",                       
                       $t2/@head,
                       $t2/@relation,
                       "_",
                       "_" 
                      ), "&#x9;")
     , "&#xA;") 
    
};

<text>{
for $t in collection("/Users/mycomputer/Documents/COMBO_for_Ancient_Greek/Greek/texts/")
let $se := <text>{$t//sentence[every $w in ./word/@* satisfies $w != ""]
                  }</text>
let $g := copy $e := $se 
                     modify (
                             for $d at $c in $e//sentence 
                             return replace value of node $d/@id with $c
                            ) 
                     return $e 
let $w := $g//word
let $d := round(count($w) * 0.8)
let $i := ($w)[$d]/parent::sentence/@id
let $rangeTrain := 1 to $i
let $fi := $g//sentence[last()]/@id
let $fe := round(count($g//sentence[@id = $i]/following::sentence//word) div 2)  
let $sd := $w[$d + $fe]/parent::sentence/@id
let $rangeDev := xs:integer($i) + 1 to $sd
let $rangeTest := xs:integer($sd) + 1 to $fi
return
 for $u in $rangeTrain (: change the range to identify the corresponding set :)
 return
 $g//sentence[@id = $u]
}</text> => local:conllu()